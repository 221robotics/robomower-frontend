FROM 221robotics/armv7hf-debian-qemu

RUN [ "cross-build-start" ]

RUN apt-get update && \
    apt-get install -y nginx php5-fpm php5-gd php5-sqlite && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir /web
WORKDIR /web
ADD tileserver.php /web/tileserver.php

COPY default /etc/nginx/sites-enabled/default

EXPOSE 80

COPY run.sh /run.sh
RUN chmod +x /run.sh
CMD [ "/run.sh" ]

RUN [ "cross-build-end" ]
